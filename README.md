# BOM and Version Catalog for androidbyexample.com Sample Code

This project contains a Version Catalog and Bill-of-Materials for all sample projects
at https://androidbyexample.com.

This is intended to reduce maintenance across terms of the course. While the concept of
version catalogs and BOMs are a great practice, for the course, I repeatedly overwrite
version 1.0 of the produced artifacts. That's not really a good idea, but makes it so
I don't need to update the artifact versions in every sample project when I update
the BOM and version catalog.

To install the BOM and Version Catalog artifacts, just run (in this project directory)

```shell
./gradlew
```

This will create a directory named `android-by-example-bom-repo` in the _parent_ directory
of this project. This isn't a good idea in general, but I didn't want to install these
in a public mvn repository as real projects might include these artifacts. Because I
will keep updating version 1.0, any real project using these (had I published them
online) would be very unstable.

If you're using BOMs/Version Catalogs, please use normal version practices and publish to
a real, _private_ mvn repository. Preferably, something like a nexus server on your
corporate network.
