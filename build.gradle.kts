// Top-level build file where you can add configuration options common to all sub-projects/modules.

// To create and publish the BOM and catalog

plugins {
    `java-platform`
    `version-catalog`
    `maven-publish`
}

catalog {
    versionCatalog {
        version("jvmTarget", "11")
        version("javaVersion", "VERSION_11")
        version("minSdk", "24")
        version("targetSdk", "33")
        version("compileSdk", "33")
        version("compose-compiler", "1.4.7")
        version("android-gradle-plugin", "8.1.0-beta01")
        version("kotlin", "1.8.21")
        plugin("android-application", "com.android.application").versionRef("android-gradle-plugin")
        plugin("android-library", "com.android.library").versionRef("android-gradle-plugin")
        plugin("kotlin", "org.jetbrains.kotlin.android").versionRef("kotlin")
        plugin("kotlin-jvm", "org.jetbrains.kotlin.jvm").versionRef("kotlin")
        plugin("ksp", "com.google.devtools.ksp").version("1.8.21-1.0.11")
    }
}

javaPlatform {
    allowDependencies() // to include the compose platform
}

dependencies {
    api(platform("androidx.compose:compose-bom:2023.05.01"))
    constraints {
        api("androidx.core:core-ktx:1.10.1")
        api("androidx.lifecycle:lifecycle-runtime-ktx:2.6.1")
        api("androidx.activity:activity-compose:1.7.2")
        api("junit:junit:4.13.2")
        api("androidx.test.ext:junit:1.1.5")
        api("androidx.test.espresso:espresso-core:3.5.1")

        api("androidx.appcompat:appcompat:1.6.1")
        api("com.google.android.material:material:1.9.0")
        api("androidx.constraintlayout:constraintlayout:2.1.4")
        api("androidx.navigation:navigation-fragment-ktx:2.5.3")
        api("androidx.navigation:navigation-ui-ktx:2.5.3")

        api("androidx.room:room-runtime:2.5.1")
        api("androidx.room:room-ktx:2.5.1")
        api("androidx.room:room-compiler:2.5.1")
    }
}


publishing {
    repositories {
        maven(uri("../android-by-example-bom-repo")) {
            name = "androidByExampleBom"
        }
    }
    publications {
        create<MavenPublication>("catalog") {
            groupId = "com.androidbyexample"
            artifactId = "version-catalog"
            version = "1.0"
            from(components["versionCatalog"])
        }
        create<MavenPublication>("bom") {
            groupId = "com.androidbyexample"
            artifactId = "bom"
            version = "1.0"
            from(components["javaPlatform"])
        }
    }
}

defaultTasks("publishAllPublicationsToAndroidByExampleBomRepository")